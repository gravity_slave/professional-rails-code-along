require 'rails_helper'

RSpec.describe AuditLog, type: :model do
 before do
   @audit_log = FactoryGirl.create(:audit_log)
end

  describe 'cration' do
   it 'can be properly created ' do
      expect(@audit_log).to be_valid
end

  end

  describe 'validations', audit: true  do
    it 'should be requierd to have a user association' do
      @audit_log.user = nil
      expect(@audit_log).to_not be_valid
    end

    it 'should always have a status ' do
      @audit_log.status = nil
      expect(@audit_log).to_not be_valid
    end

    it 'should  be required to have a start date' do
      @audit_log.start_date = nil
      expect(@audit_log).to_not be_valid
    end

    it 'should have a start date equal to  6 days prior' do
      new_audit_log=AuditLog.create(user: User.last,status: 0,start_date: 6.days.ago)
      expect(new_audit_log.start_date).to eq(Date.today - 6.days)
    end
  end
end
