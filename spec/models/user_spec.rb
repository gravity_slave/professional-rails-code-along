require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user= FactoryGirl.create(:user)
  end
describe 'creation' do

it 'can be created' do

  expect(@user).to be_valid
end

end

  describe 'creation' do
    it 'requires th phone that only  consists of integers' do
      @user.phone ='123456789'
      expect(@user).to_not be_valid
    end

    it 'requires the phone attr to only have 10 char' do
      @user.phone = "mygreatstr"
      expect(@user).not_to be_valid

    end

    it 'can not be created without first_name ' do
      @user.first_name =nil
      expect(@user).not_to be_valid
    end
    it 'can not be created without  last_name' do
      @user.last_name = nil

      @user.phone = nil
      expect(@user).not_to be_valid
    end
    it 'can not be created without phone' do

      @user.phone = nil
      expect(@user).not_to be_valid
    end

    it 'requires the ssn attr' do
      @user.ssn = nil
      expect(@user).to_not be_valid
    end

    it 'requires a company' do
      @user.company = nil
      expect(@user).not_to be_valid
    end

  end

  describe 'relationship between admins and employees', employee: true do
    it 'allows for admins to be asscociatedwith multiple employees' do
      employee1=FactoryGirl.create(:user)
      employee2=FactoryGirl.create(:user)
      admin = FactoryGirl.create(:admin_user)
      Hand.create!(user_id: admin.id, hand_id: employee1.id)
      Hand.create!(user_id: admin.id, hand_id: employee2.id)
      expect(admin.hands.count).to eq(2)

    end
  end

  end
