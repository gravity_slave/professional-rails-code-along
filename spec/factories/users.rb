FactoryGirl.define do
  sequence :email do |n|
    "test#{n}@example.com"
  end
  factory :user do
    first_name 'John'
    last_name 'Doe'
    email
    password 'password'
    password_confirmation 'password'
    phone '79523588670'
    ssn 1337
    company 'company'
  end
  factory :second_user, class: User  do
    first_name 'John'
    last_name 'Doe'
    email
    password 'password'
    password_confirmation 'password'
    phone '79523588670'
    ssn 1337
    company 'company'
    end
  factory :admin_user, class: AdminUser do
    first_name 'Admin'
    last_name 'User'
    email
    password 'password'
    password_confirmation 'password'
    phone '79523588670'
    ssn 1337
    company 'company'
  end

  factory :non_authorized_user, class: User do
    first_name 'Non'
    last_name 'Authorized'
    email
    password 'asdfasdf'
    password_confirmation 'asdfasdf'
    phone '79523588670'
    ssn 1337
    company 'company'
  end

end

