FactoryGirl.define do
  factory :post do
    date Date.today
    work_performed "Some work_performed"
    user
    daily_hours 3.5
  end

  factory :second_post, class: Post do
    date Date.yesterday
    work_performed "Some more content"
    user
    daily_hours 0.5
  end

  factory :third_post, class: Post do
    date Date.today
    work_performed "Some work_performed"
    association :user, factory: :non_authorized_user
    daily_hours 3.5
  end
end
