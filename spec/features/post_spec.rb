require 'rails_helper'

describe 'navigate' do
  let(:user) { FactoryGirl.create(:user)}
  let(:post) do
    Post.create(date: Date.today, work_performed:'work_performed', user: user, daily_hours: 3.5)
  end
  before do
    login_as(user, scope: :user)
  end
  describe 'index' do
    before do
      visit posts_path
    end

    it 'can be reached successfully' do
      expect(page.status_code).to eq(200)
    end

    it 'has a title of Posts' do
      expect(page).to have_content(/Posts/)
    end

    it 'has a list of posts' do
      post1 = FactoryGirl.build_stubbed(:post)
      post2 = FactoryGirl.build_stubbed(:second_post)
      visit posts_path
      expect(page).to have_content(/work_performed|content/)
    end

    it 'has a scope so that only post creators cansee their posts' do
      post_from_other_user =FactoryGirl.create(:third_post, work_performed: "this post shouldn't be seen")
      visit posts_path
      expect(page).not_to have_content(/this post shouldn't be seen/)

end
  end

  describe 'new' do
    it 'has a link from the homepage' do
      employee = Employee.create(first_name: 'Employee', last_name: 'Authorized', email: "employee@example.com", password: "asdfasdf", password_confirmation: "asdfasdf", phone: "5555555555")
      login_as(employee, :scope => :user)
      visit root_path

      click_link("new_post_from_nav")
      expect(page.status_code).to eq(200)
    end
  end

  describe 'delete' do
    # before do
    #   @post = FactoryGirl.create(:post, user: user)
    # end
    it 'can be deleted' do
      logout(:user)
      delete_user =FactoryGirl.create(:user)
      login_as(delete_user, scope: :user)
      post_to_delete =FactoryGirl.create(:post, user: delete_user)
      visit posts_path
      click_link("delete_post_#{post_to_delete.id}_from_index")
      expect(page.status_code).to eq(200)

    end
  end

  describe 'creation' do
    before do
      visit new_post_path

    end
    it 'has a new form that can be reached' do

      expect(page.status_code).to eq(200)
end
    it 'can be created from  new form page' do

      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: 'Some work_performed'
      fill_in 'post[daily_hours]', with:  4.5
      expect { click_on 'Save' }.to change(Post, :count).by(1)
    end

    it 'will have a user associated it' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: 'User_Association'
      fill_in 'post[daily_hours]', with:  4.5
      click_on('Save')

      expect(User.last.posts.last.work_performed).to eq('User_Association')
end
  end

  describe 'edit', pundit: true do

    it 'can be edited' do

      visit edit_post_path(post)
      fill_in 'post[date]', with: Date.today
      fill_in 'post[work_performed]', with: 'Edited content'
      click_on('Save')

      expect(page).to have_content("Edited content")

    end

    it 'can not be edited by a non authorized user' do
      logout(:user)
      @non_authorized_user = FactoryGirl.create(:non_authorized_user)
      login_as(@non_authorized_user, scope: :user)

      visit edit_post_path(post)

      expect(current_path).to eq(root_path)
end
  end

end

