require 'rails_helper'

RSpec.feature 'homepage' do
  describe 'homepage' do
    it 'can be reached successfully' do
      @user= FactoryGirl.create(:user)
      login_as(@user)
      visit root_path
      expect(page.status_code).to eq(200)

end

  end
end