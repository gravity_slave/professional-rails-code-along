require 'rails_helper'

describe 'AuditLog Feautre' do

  before do
    FactoryGirl.create(:audit_log)
    @admin_user = FactoryGirl.create(:admin_user)
    login_as(@admin_user, scope: :user)
    visit audit_logs_path
  end
  describe 'index' do
    it 'has an index page that can be reached' do
      expect(page.status_code).to eq(200)
    end

    it 'renders auditlog content' do


      expect(page).to have_content("DOE, JOHN")
    end
    it 'can not be accessed by non admin user' do
      logout(:user)
      @new_user=FactoryGirl.create(:user)
      login_as(@new_user, scope: :user)
      visit audit_logs_path
      expect(current_path).to eq(root_path)
    end
  end

end