## Overtime App

Key requirement: company need documentation that salaried employees did or did not get overtime each week

## Model
- x Post -> date:date work_performed:text
- x User - Devise
- x AdminUser -> STI
- AuditLog

## Features: 
- Approval Workflow
- SMS Sending -< link to approval or overtime input
- x Administrate admin dashboard
- Email summary to manage approval
- Needs to be documented if employee did not log overtime
- x Block non-admin and guest users
 ## UI:
 - x Bootstrap -> formatting
 - Icons from Font Awesome
 - x Update the styles for forms

## Refactor TODOS:
- Refactor  user association integration test in  post_spec
- Refactor posts/_form for  admin user with status 