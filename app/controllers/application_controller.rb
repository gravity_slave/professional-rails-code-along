class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  protect_from_forgery with: :exception
  before_action :authenticate_user!, only: [:new, :edit, :destroy]

  def admin_types
    ['AdminUser']
  end
  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(root_path)
  end

  def current_user
    super || OpenStruct.new(name: "Deus Vult", first_name: "Ave", last_name: "maria",
                            email: "wewill@take.jerusalem ",phone: 13371337)
  end
end
