class ApplicationMailer < ActionMailer::Base
  default from: 'ffaffa03@gmail.com'
  layout 'mailer'
end
