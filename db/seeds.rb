@employee = Employee.create(email: "test@test.com",
                    password: "asdfasdf",
                    password_confirmation: "asdfasdf",
                    first_name: "Jon",
                    last_name: "Snow",
                    phone: '79523588670',
                    ssn: 1228,
                    company: 'mailru')

puts "1 User created"

AdminUser.create(email: "admin@test.com",
                 password: "asdfasdf",
                 password_confirmation: "asdfasdf",
                 first_name: "Admin",
                 last_name: "Name",
                 phone: '79523588670',
                 ssn: 1322,
                 company: 'Apple')

puts "1 Admin User created"

100.times do |post|
  Post.create!(date: Date.today, work_performed: "#{post} rationale content Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 Fusce in lacus tempus, fermentum leo id, mattis ex. Suspendisse molestie ipsum urna. Nullam facilisis, risus mattis lacinia
fringilla, est nisi facilisis ex, quis iaculis lacus velit quis mauris. Morbi condimentum, odio non scelerisque ornare,
libero elit porta ligula, sit amet accumsan lorem diam nec diam. Mauris id lorem quam. Proin quis condimentum nisl.
Vivamus id ante posuere, elementum odio non, dapibus risus. Sed ut aliquam sapien. Donec volutpat tortor id semper
tincidunt. Nunc dapibus justo at dolor gravida imperdiet. Nulla libero nisi, finibus id mauris eget, lacinia pharetra justo.
 Mauris sapien nunc, faucibus vitae quam nec, posuere semper dui. Duis et lobortis nisl, malesuada iaculis mi. ",
               user: @employee, daily_hours: 2.5)
end

puts "100 Posts have been created"


AuditLog.create!(user: @employee, status: 0, start_date: (Date.today - 6.days))
AuditLog.create!(user: @employee, status: 0, start_date: (Date.today - 13.days))
AuditLog.create!(user: @employee, status: 0, start_date: (Date.today - 20.days))

puts "3 audit logs have been created"